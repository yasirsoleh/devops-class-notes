# Monday 27 May 2024

## Recap Last Week

### Devops Principles

CI/CD

- Continous Integration: code is pushed, build and tested
- Continous Delivery: new code is deployed but does not break current code
- Continous Deployment: deployment is automated

Linux CLI

- cd
- source: activate script as current shell with environment
- ./ execute program in new shell
- mv
- cp
- mkdir
- rm
- apt
- wget

Basic Git

- init
- add .
- commit -m "message"
- push
- clone
- pull

## Outline for This Week

GitLab

- Repository
- CI/CD

Jenkins

- CI/CD

Docker

- Preparation for Next Week

## This Week

### Git

SSH Key Generate

```
ssh-keygen -t rsa -C "comment"
```

SSH

```
# -T is for disable pseudo terminal
ssh -T git@gitlab.com
```

### Issues

1. assign issues
2. create new branch
3. create
