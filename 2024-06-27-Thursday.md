# Thursday 27 June 2024

## Cluster Management
### Autocompletion
```
echo "source <(kubectl completion bash)" >> ~/.bash_profile
```
### Abbreviation
```
kubectl api-resources | less
```

### View by all namespace
```
kubectl verb resource --all-namespace --show-labels -l app=microservice
```

## JSONPATH
For filtering

## Dry Rum
```
kubectl create resource -o yaml --dry-run=client > filename.yml	
```

## Run
```
kubectl run nginx --image=nginx --port=80 --expose --dry-run -o yaml > filename.yml
```

## Run everything in a folder
```
kubectl apply -f folderName/
```

## Assignment
- apache,postgres: deployment and service (in a namespace)
