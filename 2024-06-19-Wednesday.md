# Wednesday 19 June 2024

## Kubernetes

- Pod smallest
- Kind: Pod, Service, Deployment

### How to port forward

```
kubectl port-forward kind/resource_name HOST_PORT:RESOURCE_PORT
```

### How to pesist in the background

Nohup - persist, & - in the background

```
nohup kubectl port-forward kind/resource_name HOST_PORT:RESOURCE_PORT &
```
